import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import store from "./state/mainStore.ts"

import AppRouter from "./router/router"
import "./styles/main.css"

const root = document.getElementById("root");


console.log("Let Me Learn Scripts Launched");
console.log(root);

ReactDOM.render(
  <Provider store={store}>
    <AppRouter />
  </Provider>
  , root
);

