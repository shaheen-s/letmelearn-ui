import { createStore } from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import rootReducer from "./rootReducer.ts";

const _window = window as any;
export default createStore(
  rootReducer,
  composeWithDevTools()
)
