import { createReducer } from "./stateUtils/reducerUtils.ts";

const initialState = {
  name: "",
  id: ""
}

const changeName = (state: any, name: string) => {
  return {
    ...state,
    name: name
  }
}

export default createReducer(initialState, {
  CHANGE_NAME: changeName
});
