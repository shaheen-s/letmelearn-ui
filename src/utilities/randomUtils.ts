export const getRdmImage = () => {
  const imgNames = ["books", "typewriter", "glassesbook", "alignedbooks", "heartbook"]
  const img = require(`Images/${getRdmElem(imgNames)}.jpg`);
  return img.default;
}

export const getRdmElem = (arr: Array<any>) => {
  const rdmIdx = getRdmNum(arr.length);
  return arr[rdmIdx];
}

export const getRdmNum = (range?: number) => {
  const limit = 100000;
  if(range) return Math.floor(Math.random() * limit % range);
  else return Math.floor(Math.random() * limit)
}

export const getRdmColor = () => {
  const rdmColor = [0, 0, 0];
  for(let i = 0; i < rdmColor.length; i++){
    rdmColor[i] = getRdmNum(255);
  }
  return rdmColor
}
