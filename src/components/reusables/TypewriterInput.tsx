import React from "react";

interface Props {
  inType: string
  placeholder?: string
}

interface Handlers {

}

const TypewriterInput = (props: Props & Handlers) => {
  const { inType } = props;
  const placeholder = props.placeholder? props.placeholder : "";
  return (
    <div className="inline typewriterInput">
      <input type={inType} placeholder={placeholder}/>
    </div>
  )
}

export default TypewriterInput;
