import React from "react";
import { connect } from "react-redux";
import { getImage } from "@utilities/assetUtils.ts";

interface Props {
  imageUrl: string
  className?: string
  width: number
  height: number
  extension?: string
  action: (event: any) => void
}

const ActionImage = (props: Props) => {
  const {
    imageUrl, className,
    width, height,
    action
  } = props;
  let src = "";

  if(imageUrl.includes('.')) 
    src = getImage(
      imageUrl.split('.')[0], 
      imageUrl.split('.')[1]
    );
  else src = getImage(imageUrl);

  return (
    <div className={`${className ? className : ""}`}>
      <img 
      src={src} 
      onClick={action}
      width={width} 
      height={height} />
    </div>
  )
}

export default ActionImage;
