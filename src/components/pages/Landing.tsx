import React, { useState } from "react";
import { connect } from "react-redux";
import ImagedDiv from "@components/reusables/ImagedDiv";
import Centered from "@components/reusables/Centered";
import { getRdmImage } from "@utilities/randomUtils.ts";
import TypewriterInput from "@components/reusables/TypewriterInput";

interface Props {

}

interface Handlers {

}


const Landing = (props: Props & Handlers) => {
  return (
      <ImagedDiv image={getRdmImage()} colored={true}>
        <Centered>
          <div>
            <span className="landingText"> I want to learn </span>
            <TypewriterInput inType="text" placeholder="Subject..." />
          </div>
        </Centered>
      </ImagedDiv>
  )
}

const mapStateToProps = (state: any) => {
  return {

  }
}

const mapDispatchToProps = (state: any) => {
  return {

  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Landing);

