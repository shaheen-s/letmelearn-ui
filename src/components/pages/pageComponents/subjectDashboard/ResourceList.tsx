import React from "react";
import { connect } from "react-redux";
import ScrollableList from "@components/reusables/ScrollableList";

interface Props {

}

interface Handlers {

}

const ResourceList = (props: Props & Handlers) => {
  return (
    <div className="subjectResources">
      <ScrollableList items={{}} itemType="stuff" />
    </div>
  )
}

const mapStateToProps = (state: any) => {
  return {

  }
}

const mapDispachToProps = (dispatch: Function) => {
  return {

  }
}

export default connect(
  mapStateToProps,
  mapDispachToProps
)(ResourceList);

